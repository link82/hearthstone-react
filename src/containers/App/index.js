import React from 'react'
import Home from '../Home'
import CardDetail from '../CardDetail'
import About from '../About'
import Menu from '../../components/Menu'
import './App.css';

import {
  BrowserRouter as Router,
  Route
} from 'react-router-dom'

export default class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      links: [
        {
          path: '/',
          name: 'Homepage',
          component: Home,
          showInMenu: true,
          exact: true
        },
        {
          path: '/card/:id',
          name: 'CardDetail',
          component: CardDetail,
          showInMenu: false,
          exact: false
        },
        {
          path: '/about',
          name: 'About',
          component: About,
          showInMenu: true,
          exact: false
        }
      ]
    };
  }
  render(){
    return (<Router>
      <div className="App">
        <Menu links={this.state.links} />
        {this.state.links.map((link) => {
          return <Route exact={link.exact} path={link.path} component={link.component} key={link.path} />;
        })}
      </div>
    </Router>);
  }
};
