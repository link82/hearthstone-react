import React, { Component } from 'react'
import { connect } from 'react-redux'
import { setCardDetailId, getCards } from '../../reducers/cards/actions'
import { selectCurrentCard } from '../../reducers/cards/selectors'
import MultiImage from '../../components/MultiImage'
import Spinner from 'react-spinkit'
import './CardDetail.css';
import { capitalize } from '../../helpers'
import difference from 'lodash.difference'
import Breadcrumbs from '../../components/Breadcrumbs';

class CardDetail extends Component{

  // componentDidMount() {
  //   const { dispatchSetCardDetailId, card } = this.props
  //   const { id } = this.props.match.params
  //
  //   if (id) {
  //     dispatchSetCardDetailId(id)
  //   }
  // }
  //
  // componentWillReceiveProps({ card }) {
  //
  //   if (!card && card === this.props.card) {
  //     this.props.dispatchGetCards()
  //   }
  // }

  componentWillMount() {
    const { dispatchSetCardDetailId } = this.props
    const { id } = this.props.match.params

    if (id) {
      dispatchSetCardDetailId(id)
    }
  }

  _breadCrumbsLinks(){
    const { card } = this.props
    const { id } = this.props.match.params
    return [
      {
        current: false,
        path: '/',
        name: 'Home'
      },
      {
        current: true,
        path: `/card/${id}`,
        name: (card ? card.name : '')
      },
    ]
  }

  componentDidMount() {
    const { dispatchGetCards, card } = this.props
    if (!card) {
      console.log('fetching cards from detail')
      dispatchGetCards()
    }
  }

  _cardImages(){
    const { card } = this.props

    return [
        {
          url: card.img,
          name: 'Basic card',
          golden: false
        },
        {
          url: card.imgGold,
          name: 'Golden card',
          golden: true
        }
      ];
  }

  render() {
    const { card } = this.props
    if (card) {
      return (
        <div className='card-detail'>
          <Breadcrumbs links={this._breadCrumbsLinks()} />
          <h1>
            { card.name }
          </h1>
          <div className='container'>
            <div className='row'>
              <div className='col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2'>
                <div className='card-sheet'>
                  <div className='col-xs-12 col-sm-12 col-md-6 col-lg-6 card-image'>
                    <figure>
                      <MultiImage currentImage={this._cardImages()[0]} images={this._cardImages()} />
                    </figure>
                  </div>
                  <div className='col-xs-12 col-sm-12 col-md-6 col-lg-6 card-data'>
                    <ul className='list-unstyled'>
                      {difference(Object.keys(card), ['img', 'locale', 'collectible','imgGold']).map((key) => {
                        return (<li key={key}>
                          <strong>{capitalize(key)}</strong>
                          <span>{card[key]}</span>
                        </li>);
                      })}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    } else {
      return (
        <div className='card-detail'>
          <div className='loader'>
            <Spinner name='pacman' color='#ffeb3b' />
            <h3>loading your card...</h3>
          </div>
        </div>
      )
    }
  }

}

const mapStateToProps = (state) => ({
  card: selectCurrentCard(state),
  currentCardId: state.cards.currentCardId
})

const mapDispatchToProps = (dispatch) => {
  return {
    dispatchSetCardDetailId: (id) => {
      dispatch(setCardDetailId(id))
    },
    dispatchGetCards: () => {
      dispatch(getCards())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CardDetail);
