import React from 'react';
import CardsList from '../../components/CardsList';
import Breadcrumbs from '../../components/Breadcrumbs';

export default class Home extends React.Component{

  _breadCrumbsLinks(){
    return [
      {
        current: true,
        path: '/',
        name: 'Home'
      },
    ];
  }

  render(){
    return (
      <div className='home'>
        <Breadcrumbs links={this._breadCrumbsLinks()} />
        <h1>Hearthstone cards</h1>
        <CardsList />
      </div>
    );
  }
}
