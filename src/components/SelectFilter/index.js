import React from 'react'

export default (props) => {
  return (
    <label className='select-filter'>
      <span className='filter-label'>{props.label}</span>
      <select name={props.listName} className='form-control' value={props.currentItem} onChange={props.onChange}>
        {
          props.listItems.map((item) => {
            return <option className='filter-option' value={item} key={item}>{item}</option>
          })
        }
      </select>
    </label>
    );
};
