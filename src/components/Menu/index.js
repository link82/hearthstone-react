import React from 'react'
import {
  Link
} from 'react-router-dom'


export default (props) => (
  <nav className="navbar navbar-inverse navbar-fixed-top">
    <div className="container">

      <div className="navbar-header">
        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
          <span className="sr-only">Toggle navigation</span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
          <span className="icon-bar"></span>
        </button>
        <a className="navbar-brand" href="/">Hearthstone</a>
      </div>

      <div id="navbar" className="collapse navbar-collapse">
        <ul className="nav navbar-nav">
          {props.links.map((link) => {
            return (link.showInMenu ? <li className="active" key={link.path}><Link to={link.path}>{link.name}</Link></li> : null);
          })}
        </ul>

      </div>
    </div>
  </nav>
);
