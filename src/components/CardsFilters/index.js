import React from 'react'
import SelectFilter from '../SelectFilter'
import InputFilter from '../InputFilter'
import Button from '../Button'

export default (props) => {

  return (
    <form action=''>
      <fieldset className='form-group'>
        {Object.keys(props.filters).map((filterName) => {
          let filter = props.filters[filterName]
          switch(filter.type){
            case 'select':
              return (<SelectFilter
                listName={filter.name}
                label={filter.label}
                listItems={filter.items}
                currentItem={filter.currentItem}
                onChange={(e) => props.updateField(filterName, e.target.value)}
                key={filterName} />);
            case 'input':
              return (<InputFilter
                inputName={filter.name}
                label={filter.label}
                currentValue={filter.currentItem}
                onChange={(e) => props.updateField(filterName, e.target.value)}
                key={filterName} />);
            default:
              return null;
          }

        })}

        {(props.resetButton && props.resetFunction) ? <Button onClick={props.resetFunction} label='Reset' /> : '' }

      </fieldset>
    </form>
  );
}
