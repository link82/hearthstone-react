import React from 'react'
import {
  Link
} from 'react-router-dom'

import './Breadcrumbs.css'

export default (props) => {
  return (
    <ul className='list-inline breadcrumbs'>
      {props.links.map(link => {
        const classes = link.current ? 'active' : ''
        return <li><a href={link.path} className={classes}>{link.name}</a></li>;
      })}
    </ul>
  )
}
