import React from 'react'

export default (props) => {
  return (
    <label className='input-filter'>
      <span className='filter-label'>{props.label}</span>
      <input type='text' className='form-control' name={props.inputName} value={props.currentValue} onChange={props.onChange} />
    </label>
  );
};
