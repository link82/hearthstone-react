import React from 'react'
import './MultiImage.css'
export default class MultiImage extends React.Component{

  constructor(props){
    super(props)
    this.state = {
      currentImage: props.currentImage
    }
  }

  _setCurrentImage(image){
    this.setState({currentImage: image})
  }

  render(){

    return (
      <div className='multi-image'>
        <img src={this.state.currentImage.url} alt={this.state.currentImage ? this.state.currentImage.name : ''} />
        <ul className='list-inline image-selector'>
          {this.props.images.map((img, index) => {
            let linkClasses = []
            linkClasses.push(this.state.currentImage && this.state.currentImage.url === img.url ? 'active' : '');
            linkClasses.push(img.golden ? 'golden' : 'basic');
            return <li key={index}><a onClick={_ => this._setCurrentImage(img)} className={linkClasses.join(' ')}>&nbsp;</a></li>;
          })}
        </ul>
      </div>
    );


  }
}
