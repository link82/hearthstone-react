import React from 'react'

export default (props) => {
  return (
    <button className='btn btn-default btn-info' onClick={(event) => {event.preventDefault(); props.onClick()}}>{props.label}</button>
  );
}
