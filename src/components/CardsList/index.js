import React from 'react'
import { connect } from 'react-redux'

import Card from '../Card'
import CardsFilters from '../CardsFilters'

import { getCards } from '../../reducers/cards/actions'

class CardsList extends React.Component{
  constructor(props){
    super(props);
      this.state = {
      filters: {
        'cardSet': {
          name: 'cardSet',
          type: 'select',
          label: 'Set di carte',
          items: ['', 'Basic', 'Blackrock Mountain', 'Classic', 'Goblins vs Gnomes', 'Hall of Fame', 'Hero Skins', 'Journey to Un\'Goro', 'Mean Streets of Gadgetxan', 'Naxxramas', 'One Night in Karazhan','The Grand Tournament','The League of Explorers','Whispers of the Old Gods'],
          currentItem: ''
        },
        'faction': {
          type: 'select',
          name: 'faction',
          label: 'Fazione',
          items: ['', 'Neutral','Horde','Alliance'],
          currentItem: ''
        },
        'rarity': {
          type: 'select',
          name: 'rarity',
          label: 'Rarità',
          items: ['', 'Free','Common','Rare','Epic','Legendary'],
          currentItem: ''
        },
        'name': {
          type: 'input',
          name: 'name',
          label: 'Nome',
          currentItem: ''
        }
      }
    }
  }

  componentDidMount() {
    const { dispatchGetCards, cards } = this.props
    if (Array.isArray(cards) && !cards.length) {
      console.log('AYYYYYY i\'m fetching API FROM LIST')
      dispatchGetCards()
    }
  }

  _filterValue(filterName){
    return this.state.filters[filterName].currentItem;
  }

  _updateField(key, value){
    let newFilters = Object.assign({}, this.state.filters);
    newFilters[key].currentItem = value
    this.setState({currentFilter: value});
  }

  _activeFilters(){
    return Object.keys(this.state.filters).map((filter_name) => {
      let filter = this.state.filters[filter_name]
      return filter.currentItem !== '';
    }).reduce((a,b) => {
      return a && b;
    },true);
  }

  _clearFilters(){
    console.log('Resetting filters');
    let emptyFilters = {}
    Object.keys(this.state.filters).forEach(key => {
      const value = this.state.filters[key]
      emptyFilters[key] = Object.assign({}, value, {currentItem: ''})
    })
    this.setState({filters: emptyFilters})
  }

  _filterCards = (card) =>{
    return Object.keys(this.state.filters).map(key => {
      const filter = this.state.filters[key]
      if(filter.type === 'input')
        return (filter.currentItem === '' || card[key].toLowerCase().includes(filter.currentItem.toLowerCase()))
      else //exact value
        return (filter.currentItem === '' || filter.currentItem === card[key])
    }).reduce((a,b) => {return a && b;},true);
  }

  render(){
    return (
        <div className='container card-list'>
          <CardsFilters filters={this.state.filters} updateField={this._updateField.bind(this)} resetButton={true} resetFunction={this._clearFilters.bind(this)} />
          <div className='row'>
            {this.props.cards.filter(this._filterCards).map((card) => {return (<Card card={card} key={card.cardId} />);})}
          </div>
        </div>
    );
  }
}

const mapStateToProps = ({ cards }) => ({
  cards: cards.list
})

const mapDispatchToProps = (dispatch) => {
  return {
    dispatchGetCards: () => {
      dispatch(getCards())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CardsList);
