import React from 'react';
import {
  Link
} from 'react-router-dom'

export default class Card extends React.Component{
  render(){
    return (
      <div className='col-md-4 col-sm-6 col-xs-12'>
        <Link to={`/card/${this.props.card.cardId}`} key={this.props.card.cardId}>
          <img src={this.props.card.img} alt={this.props.card.name} />
          <h4>{this.props.card.name}</h4>
        </Link>
      </div>
    );
  }
}
