import { put, call, takeEvery } from 'redux-saga/effects'
import Api from '../../api'

import { contentsActions, contentsTypes } from '../modules/Contents'

function* getData () {
  try {
    const result = yield call(Api.getData, 'burgerKingAppGetCategoriesAndProducts')
    yield put(contentsActions.productsRequestSuccess(result))
  } catch (error) {
    yield put(contentsActions.productsRequestFailure(error))
  }
}

export default function* productsSaga () {
  yield takeEvery(contentsTypes.PRODUCTS_REQUEST, getData)
}
