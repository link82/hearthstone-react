import cardSaga from './card'
import cardsSaga from './cards'

export default {
  cardSaga,
  cardsSaga,
}
