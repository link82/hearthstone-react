import { put, call, takeEvery, select } from 'redux-saga/effects'
import Api from '../../api'

import { productActions, productTypes, productSelectors } from '../modules/Product'

function* getData () {
  try {
    const id = yield select(productSelectors.selectProductId)
    const product = yield select(productSelectors.selectProduct)
    // check if we have already stored product
    if (product) {
      yield put(productActions.productRequestSuccess(product))
    } else {
      const result = yield call(Api.getData, `burgerKingAppGetProductDetail?itemId=${id}`);
      yield put(productActions.productRequestSuccess(result))
    }
  } catch (error) {
    yield put(productActions.productRequestFailure(error))
  }
}

export default function* productSaga () {
  yield takeEvery(productTypes.PRODUCT_REQUEST, getData)
}
