//reducers
import { SET_CARDS, SET_CARD_DETAIL_ID } from './actions'

const initialState = {
  list: [],
  currentCardId: false
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CARDS:
      return {
        ...state,
        list: action.cards
      };
    case SET_CARD_DETAIL_ID:
      return {
        ...state,
        currentCardId: action.id
      };
    default:
      return state;
  }
}
