import { API_URL } from '../../constants'

export const SET_CARDS = 'SET_CARDS'
export const SET_CARD_DETAIL_ID = 'SET_CARD_DETAIL_ID'

export function getCards(id = -1){
  return dispatch => {
    fetch(`${API_URL}/cards?collectible=1&locale=itIT`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'X-Mashape-Key': 'IkjIWCBpnUmshF5hJWnJK2nTdvZRp1TGXNbjsnYXJ24Bdud3f2'
      },
    })
    .then(res => res.json())
    .then(res => Object.values(res).reduce((a,b) => a.concat(b)))
    .then(cards => dispatch(setCards(cards)))
  };
}

//create action invoked
export function setCards(cards) {
  return {
    type: SET_CARDS,
    cards
  }
}

export function setCardDetailId(id) {
  return {
    type: SET_CARD_DETAIL_ID,
    id
  }
}
