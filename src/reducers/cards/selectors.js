const selectCurrentCard = (state) => {
  if (!state.cards.currentCardId) {
    return false;
  }

  if (Array.isArray(state.cards.list) && state.cards.list.length) {
    return state.cards.list.find(c => c.cardId === state.cards.currentCardId);
  } else {
    return false;
  }
}

export {
  selectCurrentCard
}
