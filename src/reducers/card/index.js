//reducers
import { SET_CURRENT_CARD } from './actions'

const initialState = {}

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_CURRENT_CARD:
      return action.card;
    default:
      return state;

  }
}
