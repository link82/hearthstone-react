export const SET_CURRENT_CARD = 'SET_CURRENT_CARD'

export function setCurrentCard(card){
  return {
    type: SET_CURRENT_CARD,
    card
  }
}
